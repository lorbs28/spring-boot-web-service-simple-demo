# Spring Boot Web Service Simple Demo

This is a simple demo project that shows how to consumer an API endpoint using jQuery on the frontend via a HTML file.  The API endpoint is built using Spring Boot.