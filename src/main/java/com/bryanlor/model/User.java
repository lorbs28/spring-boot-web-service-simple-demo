package com.bryanlor.model;

public class User {
	private final String name;
	private final String age;
	
	public User(String name, String age) {
		this.name = name;
		this.age = age;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getAge() {
		return this.age;
	}
}
