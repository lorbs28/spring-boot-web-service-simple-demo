package com.bryanlor.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bryanlor.model.User;

@RestController
public class UserController {

	@RequestMapping("/register")
	@ResponseBody
	public User registerUser(@RequestParam(value="name", required=false) String name, @RequestParam(value="age", required=false) String age) {
		return new User(name, age);
	}
}
